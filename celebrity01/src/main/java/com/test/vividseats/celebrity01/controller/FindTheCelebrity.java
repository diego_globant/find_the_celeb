package com.test.vividseats.celebrity01.controller;

import java.util.ArrayList;

import com.test.vividseats.celebrity01.model.Person;

/**
 * Test Vivid Seats!
 *
 */
public class FindTheCelebrity 
{
	static String[] files = {"C:/temp/knowns.csv","C:/temp/people.csv"};
    public static void main( String[] args )
    {
        ArrayList<Person> people = UtilPersonManage.obtainPersonList(files);
        UtilPersonManage.printPeopleArray("From all these people: ", people);
        UtilPersonManage.printPeopleArray("\nThe celebrities are:\n", UtilPersonManage.obtainCelebrity(people));
    }
}
