package com.test.vividseats.celebrity01.model;

import java.util.Arrays;

public class Person {
	private String id;
	private String name;
	private String lastName;
	private String address;
	private String age;
	private String gender;
	// This is an array of ids corresponding to the people the current person knows. It's stored in a separate file.
	private String[] knows;
	
	public Person(String id, String name, String lastName,	
				  String address, String age, String gender) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.age = age;
		this.gender = gender;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String[] getKnows() {
		return knows;
	}
	public void setKnows(String[] knows) {
		this.knows = knows;
	}
	
	public String toString() {
		return "Name: " + name + " Last name: " + lastName + " Address: " + address +
				" Age: " + age + " Gender: " + gender + " Knows: " + Arrays.toString(knows);
	}
	
}
