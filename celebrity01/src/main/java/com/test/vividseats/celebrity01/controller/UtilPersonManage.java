package com.test.vividseats.celebrity01.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import com.test.vividseats.celebrity01.model.Person;

public class UtilPersonManage {
	
	public static ArrayList<Person> obtainPersonList(String[] files) {
	
		Reader in;
		ArrayList<Person> personArray = new ArrayList<Person>();
		ArrayList<String[]> knownsArray = new ArrayList<String[]>();
		
		try {
			in = new FileReader(files[0]);
			Iterable<CSVRecord> rows = CSVFormat.DEFAULT.parse(in);
			
			for (CSVRecord row : rows) {
				String[] tempKnows = new String[row.size()];
				for(int i = 0;i<row.size();i++) {
					tempKnows[i] = row.get(i);
				}
				knownsArray.add(tempKnows);
			}
			
			in = new FileReader(files[1]);
			rows = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
			
			for (CSVRecord row : rows) {
				String[] tempKnows;
				String tempId = row.get("id");
				Person tempPerson = new Person( tempId,
												row.get("name"),
												row.get("lastName"),
												row.get("address"),
												row.get("age"),
												row.get("gender"));
				tempKnows = knownsArray.stream()
						.filter(knows -> tempId.equals(knows[0]))
						.findAny()
						.orElse(null);
				tempKnows = Arrays.copyOfRange(tempKnows, 1, tempKnows.length);
				if (Arrays.asList(tempKnows).stream().allMatch(value -> value.equals(""))) 
					tempKnows = null;
				tempPerson.setKnows(tempKnows);
				personArray.add(tempPerson);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return personArray;
	}
	
	public static ArrayList<Person> obtainCelebrity(ArrayList<Person> people) {
		ArrayList<Person> celebrities = new ArrayList<Person>();
		List<Person> possibleCelebrities = people.stream()
				.filter(person -> person.getKnows() == null)
				.collect(Collectors.toList());
		
		for (Person possibleCeleb : possibleCelebrities) {
			Boolean isCeleb = true;
			for (Person person : people) {
				if(person.getId() != possibleCeleb.getId() && person.getKnows() != null)
					isCeleb = isCeleb && Arrays.asList(person.getKnows()).contains(possibleCeleb.getId());
			}
			if (isCeleb) 
				celebrities.add(possibleCeleb);
		}
		
		return new ArrayList<Person>(celebrities);
	}
	
	public static void printPeopleArray(String message, ArrayList<Person> people) {
		System.out.println(message);
		for (int i = 0; i < people.size(); i++) {
			System.out.println(people.get(i).toString());
		}
	}
}
