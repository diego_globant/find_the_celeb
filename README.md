To run the project:
1- Make sure that Maven is installed, you can check it by running "mvn --version" in the command prompt.
2- Clone or download the repo, preferably in the eclipse-workspace folder, but any folder will work.
3- Import the project by selecting the option File->Import->Maven->Existing Maven Projects and then click on the Browse button, select the folder celebrity01 and click the button Select Folder.
4- Optional: If an error in the class UtilPersonManage.java shows up, simply go to the first error line and place the cursor over the problematic (underlined) expression and wait for the context dialog to appear. After this, click the option "Change project compliance and JRE to 1.8" and the error will be gone.
5- Create a folder called temp in your C: drive, and place in this folder the .csv files (people.csv & knowns.csv) located in the temp folder from the repo.
6- Run the application by simply right-clicking the project name in eclipse and selecting the option Run As->Java Application.

